clear all
clc

load("Data\rfsoc_nn_training.mat");
input = reshape(x(1,:,:),1,50*136320);
targets = reshape(y(1,:,:),1,50*136320);



MM = 4;
for q=0:MM
       Shifted(q+1,:)= circshift(input,q);
       Shifted(1:q) = 0;
   
end



for q=0:MM
   IN(q+1,:) = real(Shifted(q+1,:));
end

for q=0:MM
   IN(end+1,:) = imag(Shifted(q+1,:));
end




OUT = [real(targets); imag(targets)];

net = feedforwardnet([10,5,5]);

net.trainParam.epochs = 100;
net = train(net, IN,OUT);

Y = sim(net,IN);

net_out = Y(1,:)+1i*Y(2,:);

plot(abs(input),abs(targets),'.')
hold on
plot(abs(input),abs(net_out),'.')

nmse_NN = nmse(targets,net_out)


function error = nmse(y,y2)
    error=10*log10(sum((abs(y-y2).^2))/sum(abs(y).^2));
end


